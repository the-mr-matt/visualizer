float lowPass = 0;
float prevLowPass = 0;

float gain = 0;
float prevGain = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  lowPass = analogRead(0);
  gain = analogRead(1);

  if (abs(lowPass - prevLowPass) > 2)
  {
    Serial.println("LOWPASS");
    Serial.println(lowPass);
  }

  if(abs(gain - prevGain) > 2)
  {
    Serial.println("GAIN");
    Serial.println(gain);
  }

  prevLowPass = lowPass;
  prevGain = gain;

  delay(100);
}

